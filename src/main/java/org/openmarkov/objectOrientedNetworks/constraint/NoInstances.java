package org.openmarkov.objectOrientedNetworks.constraint;

import java.util.ArrayList;

import org.openmarkov.core.action.PNEdit;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.NotEnoughMemoryException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.constraint.ConstraintBehavior;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.constraint.UtilConstraints;
import org.openmarkov.core.model.network.constraint.annotation.Constraint;
import org.openmarkov.objectOrientedNetworks.OOBNet;
import org.openmarkov.objectOrientedNetworks.action.AddInstanceEdit;

@Constraint (name = "NoInstances", defaultBehavior = ConstraintBehavior.YES)
public class NoInstances extends PNConstraint {

	@Override
	public boolean checkProbNet(ProbNet probNet) {
		return !(probNet instanceof OOBNet) || ((OOBNet)probNet).getInstances().isEmpty();
	}

	@Override
	public boolean checkEdit(ProbNet probNet, PNEdit edit)
			throws NotEnoughMemoryException, NonProjectablePotentialException,
			WrongCriterionException {
		ArrayList<PNEdit> edits = UtilConstraints.getEditsType (edit, AddInstanceEdit.class);
		return edits.isEmpty();
	}
	
	@Override
	protected String getMessage() {
		return "there should be no instances";
	}
	

}
