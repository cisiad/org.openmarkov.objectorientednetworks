/*
* Copyright 2012 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/
package org.openmarkov.objectOrientedNetworks.gui;

/**
 * This class defines the constants used to identify the actions invoked by the
 * user
 * 
 * @author Iñigo
 *
 */
public class ActionCommands
{
    /**
     * Action invoked when the user wants to instantiate an object.
     */
    public static final String INSTANCE_CREATION = "InstanceCreation";    
}
