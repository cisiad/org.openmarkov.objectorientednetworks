/*
 * Copyright 2011 CISIAD, UNED, Spain
 *
 * Licensed under the European Union Public Licence, version 1.1 (EUPL)
 *
 * Unless required by applicable law, this code is distributed
 * on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.objectOrientedNetworks.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.openmarkov.core.exception.ParserException;
import org.openmarkov.core.exception.ProbNodeNotFoundException;
import org.openmarkov.core.io.ProbNetInfo;
import org.openmarkov.core.io.format.annotation.FormatType;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.ProbNode;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.io.probmodel.strings.XMLAttributes;
import org.openmarkov.io.probmodel.strings.XMLTags;
import org.openmarkov.objectOrientedNetworks.InstanceLink;
import org.openmarkov.objectOrientedNetworks.OOBNet;
import org.openmarkov.objectOrientedNetworks.io.strings.OOXMLTags;

/**
 * @author manuel
 * @author mpalacios Properties object was deprecated for basic
 *         adittionalProperties. ProbNet initialization was fixed. Method to get
 *         network comments was added.
 */
//@FormatType(name = "OOBNReader", extension = "pgmx", description="OpenMarkov OOBN", role = "Reader")
public class OOBNReader extends org.openmarkov.io.probmodel.PGMXReader {

	// Attributes
	/**
	 * Attribute that points to the unique instance of this object (singleton
	 * pattern)
	 */
	protected static OOBNReader reader = null;

	protected String version;

	// Constructor
	/** Private constructor (singleton pattern) */
	private OOBNReader() {
	}

	// Methods
	/**
	 * Singleton pattern.
	 * 
	 * @return <code>Reader</code>
	 */
	public static OOBNReader getUniqueInstance() {
		if (reader == null) {
			reader = new OOBNReader();
		}
		return reader;
	}
	
    /**
     * @param netName
     *            = path + network name + extension. <code>String</code>
     * @return The <code>ProbNet</code> readed or <code>null</code>
     */	
	@Override
    public ProbNetInfo loadProbNet(InputStream file, String netName) throws ParserException {
        SAXBuilder builder = new SAXBuilder();
        Document document = null;
        ProbNet probNet = null;
        ArrayList<EvidenceCase> evidence = null;
        try {
            document = builder.build(file);
        } catch (JDOMException e) {
            throw new ParserException("Can not parse XML document " + netName
                    + ".");
        } catch (IOException e) {
            throw new ParserException("General Input/Output error reading "
                    + netName + ".");
        }
        Element root = document.getRootElement();
        version = root.getAttributeValue(XMLAttributes.FORMAT_VERSION
                .toString());
        // Read ProbNet
        Element xMLProbNet = root.getChild(XMLTags.PROB_NET.toString());
        if (xMLProbNet != null) {// Read prob net
            // = xMLProbNet.getAttribute(XMLAttributes.TYPE.toString());
            NetworkType networkType = getNetworkType(xMLProbNet);
            probNet = new ProbNet(networkType);
            getAdditionalConstraints(probNet, xMLProbNet);

            // TODO Read Inference options

            // TODO Read Policies
            probNet = getConstraints(xMLProbNet, probNet);
            probNet.setComment(getComment(xMLProbNet));
            probNet.setName(FilenameUtils.getName(netName));
            getVariables(xMLProbNet, probNet);
            getLinks(xMLProbNet, probNet);
            getPotentials(xMLProbNet, probNet);
            getPRM(netName, xMLProbNet, probNet);
            getAgents(xMLProbNet, probNet);
            getDecisionCriteria(xMLProbNet, probNet);

            // Read Evidence
            Element xMLEvidence = root.getChild(XMLTags.EVIDENCE.toString());
            evidence = getEvidence(xMLEvidence, probNet);
        }
        return new ProbNetInfo(probNet, evidence);
    }
	
	/**
	 * @param netName 
	 * @param root
	 *            . <code>Element</code>
	 * @param probNet
	 *            . <code>ProbNet</code>
	 * @throws ParserException
	 */
	@SuppressWarnings("unchecked")
	protected void getPRM(String netName, Element root, ProbNet probNet)
			throws ParserException {

		Element xmlPrmRoot = root.getChild(OOXMLTags.PRM.toString());

		if (xmlPrmRoot != null) {
			Element xmlInstancesRoot = xmlPrmRoot.getChild(OOXMLTags.INSTANCES.toString());
			List<Element> xmlInstances = xmlInstancesRoot.getChildren();

			for (Element xmlInstance : xmlInstances) {
				String name = xmlInstance.getAttributeValue("name");
				boolean isInput = Boolean.parseBoolean(xmlInstance.getAttributeValue("isInput"));
				String folder = new File(netName).getParent();  
				ProbNet classNet =  loadProbNet(folder + "\\" + xmlInstance.getAttributeValue("class")).getProbNet();
				ArrayList<ProbNode> instanceNodes = new ArrayList<ProbNode>(); 
				//build this list from current node list and classNet
				for(ProbNode probNode: classNet.getProbNodes())
				{
					try {
						instanceNodes.add(probNet.getProbNode(name + "." + probNode.getName()));
					} catch (ProbNodeNotFoundException e) {
						throw new ParserException(e.getMessage());
					}
				}
					
			}
			Element xmlInstanceLinksRoot = xmlPrmRoot.getChild(OOXMLTags.INSTANCE_LINKS.toString());
			if (xmlInstanceLinksRoot != null) {			
			List<Element> xmlInstanceLinks = xmlInstanceLinksRoot.getChildren();
				for (Element xmlInstanceLink : xmlInstanceLinks) {
					String sourceName = xmlInstanceLink.getAttributeValue("source");
					String destName = xmlInstanceLink.getAttributeValue("destination");
					String paramName = xmlInstanceLink.getAttributeValue("parameter");
					InstanceLink link = new InstanceLink(((OOBNet)probNet).getInstances()
							.get(sourceName), ((OOBNet)probNet).getInstances().get(
							destName), ((OOBNet)probNet).getInstances().get(destName)
							.getSubInstances().get(paramName));
					((OOBNet)probNet).addInstanceLink(link);
				}
			}
			
		}
	}	
}
