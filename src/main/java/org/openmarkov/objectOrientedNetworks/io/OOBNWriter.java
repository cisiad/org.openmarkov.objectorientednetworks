/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.objectOrientedNetworks.io;

import org.jdom.Attribute;
import org.jdom.Element;
import org.openmarkov.core.io.format.annotation.FormatType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.io.probmodel.PGMXWriter;
import org.openmarkov.io.probmodel.strings.XMLTags;
import org.openmarkov.objectOrientedNetworks.Instance;
import org.openmarkov.objectOrientedNetworks.InstanceLink;
import org.openmarkov.objectOrientedNetworks.OOBNet;
import org.openmarkov.objectOrientedNetworks.io.strings.OOXMLTags;

/**
 * @author mkpalacio
 * @author marias
 * @version 1.0
 */
//@FormatType(name = "OOBNWriter", extension = "pgmx", description="OpenMarkov OOBN", role = "Writer")
public class OOBNWriter extends PGMXWriter {
	// Attributes
	/**
	 * Attribute that points to the unique instance of this object (singleton
	 * pattern)
	 */
	protected static OOBNWriter writer = null;
	/**
	 * The version format
	 */
	private static String FORMAT_VERSION_NUMBER = "0.2.0";

	// Methods
	/**
	 * Singleton pattern.
	 * 
	 * @return <code>Reader</code>
	 */
	public static OOBNWriter getUniqueInstance() {
		if (writer == null) {
			writer = new OOBNWriter();
		}
		return writer;
	}
	
    protected void getProbNetChildren(ProbNet probNet, Element probNetElement) {
        getAdditionalConstraints(probNet, probNetElement, new Element(
                XMLTags.ADDITIONAL_CONSTRAINTS.toString()));
        getProbNetComment(probNet, probNetElement,
                new Element(XMLTags.COMMENT.toString()));
        getLanguage(probNet, probNetElement,
                new Element(XMLTags.LANGUAGE.toString()));
        getVariables(probNet, probNetElement,
                new Element(XMLTags.VARIABLES.toString()));
        getLinks(probNet, probNetElement, new Element(XMLTags.LINKS.toString()));
        getPotentials(probNet, probNetElement,
                new Element(XMLTags.POTENTIALS.toString()));
        getPRM(probNet, probNetElement,
                new Element(OOXMLTags.PRM.toString()));
        getAgents(probNet, probNetElement, new Element(XMLTags.AGENTS.toString()));
        getDecisionCriteria(probNet, probNetElement, 
                new Element(XMLTags.DECISION_CRITERIA.toString()));
    }
    
    protected void getPRM(ProbNet probNet, Element probNetElement,
			Element prmElement) {
		
		if (((OOBNet)probNet).getInstances().size() > 0) {
			Element instancesElement = new Element(OOXMLTags.INSTANCES.toString());
			for (Instance instance : ((OOBNet)probNet).getInstances().values()) {
				Element instanceElement = new Element(OOXMLTags.INSTANCE.toString());
				instanceElement.setAttribute(new Attribute("name", instance.getName()));
				instanceElement.setAttribute(new Attribute("class", instance.getClassNet().getName()));
				instanceElement.setAttribute(new Attribute("isInput", Boolean.toString(instance.isInput())));
				instancesElement.addContent(instanceElement);
			}
			prmElement.addContent(instancesElement);

			if (((OOBNet)probNet).getInstanceLinks().size() > 0) {
				Element instanceLinksElement = new Element(OOXMLTags.INSTANCE_LINKS.toString());
				for (InstanceLink instanceLink : ((OOBNet)probNet).getInstanceLinks()) {
					Element instanceLinkElement = new Element(OOXMLTags.INSTANCE_LINK.toString());
					instanceLinkElement.setAttribute(new Attribute("source", instanceLink.getSourceInstance().getName()));
					instanceLinkElement.setAttribute(new Attribute("destination", instanceLink.getDestInstance().getName()));
					instanceLinkElement.setAttribute(new Attribute("parameter", instanceLink.getDestSubInstance().getName()));
					instanceLinksElement.addContent(instanceLinkElement);
				}
				prmElement.addContent(instanceLinksElement);
			}
			probNetElement.addContent(prmElement);
		}		
	}	
    
  
}