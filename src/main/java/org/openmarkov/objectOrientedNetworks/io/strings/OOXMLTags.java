/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.objectOrientedNetworks.io.strings;

import java.io.Serializable;

public enum OOXMLTags implements Serializable {
	INSTANCE("Instance"),
	INSTANCE_LINK("InstanceLink"),
	INSTANCE_LINKS("InstanceLinks"),
	INSTANCE_NODE("InstanceNode"),
	INSTANCE_NODES("Nodes"),
	INSTANCES("Instances"),
	PRM("Prm")
	;
	
	private int type;
	
	private String name;
	
	OOXMLTags(String name) {
		this.name = name;
		this.type = this.ordinal();
	}
	
	public String toString() {
		return name;
	}
	
	public int getType() {
		return type;
	}
	
}
